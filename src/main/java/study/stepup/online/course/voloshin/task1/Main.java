package study.stepup.online.course.voloshin.task1;


public class Main {

    public static void main(String[] args) throws NoChangesToUndoException {
        Account acc = new Account("Алексей");
        System.out.println(acc);
        acc.setBalance(Currency.USD, 100);
        System.out.println(acc);
        acc.setOwnerName("Иван");
        System.out.println(acc);
        acc.setBalance(Currency.RUB, 3000);
        System.out.println(acc);
        Account.AccountState save1 = acc.saveAccountState();
        System.out.println("- First savepoint");
        acc.setBalance(Currency.USD, 500);
        System.out.println(acc);
        Account.AccountState save2 = acc.saveAccountState();
        System.out.println("- Second savepoint");
        acc.undo();
        System.out.println("Undo");
        System.out.println(acc);

        System.out.println("Undo");
        acc.undo();
        System.out.println(acc);

        System.out.println("Undo");
        acc.undo();
        System.out.println(acc);
        System.out.println("Can undo: " + acc.canUndo());

        System.out.println("Undo");
        acc.undo();
        System.out.println(acc);
        System.out.println("Can undo: " + acc.canUndo());

        System.out.println("Restoring first savepoint");
        acc.restoreAccountState(save1);
        System.out.println(acc);
        System.out.println("Can undo: " + acc.canUndo());
    }
}
