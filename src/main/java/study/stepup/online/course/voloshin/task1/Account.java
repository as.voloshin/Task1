package study.stepup.online.course.voloshin.task1;

import java.util.HashMap;
import java.util.Stack;

public class Account {
    private String ownerName;
    private HashMap<Currency, Integer> balances = new HashMap<>();
    private Stack<AccountState> history = new Stack<>();


    public Account(String ownerName) throws IllegalArgumentException {
        setOwnerName(ownerName, false);
    }


    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) throws IllegalArgumentException {
        setOwnerName(ownerName, true);
    }

    private void setOwnerName(String ownerName, boolean addToHistory) throws IllegalArgumentException {
        if ((ownerName == null) || ownerName.equals("")) {
            throw new IllegalArgumentException("Имя владельца счёта не может быть null или пустым.");
        }
        if (addToHistory) {
            AccountState state = this.saveAccountState();
            this.history.push(state);
        }
        this.ownerName = ownerName;
    }

    public Integer getBalance(Currency currency) {
        Integer bal = this.balances.get(currency);
        return bal;
    }

    public HashMap<Currency, Integer> getAllBalances() {
        return (HashMap<Currency, Integer>) this.balances.clone();
    }

    public void setBalance(Currency currency, Integer balance) throws IllegalArgumentException {
        setBalance(currency, balance, true);
    }

    private void setBalance(Currency currency, Integer balance, boolean addToHistory) throws IllegalArgumentException {
        if (balance < 0) {
            throw new IllegalArgumentException("Количество валюты не может быть отрицательным.");
        }
        if (addToHistory) {
            AccountState state = this.saveAccountState();
            this.history.push(state);
        }
        this.balances.put(currency, balance);
    }

    public AccountState saveAccountState() {
        return new AccountState(this);
    }

    private void restoreAccountState(AccountState accountState, boolean clearHistory) {
        setOwnerName(accountState.getOwnerName(), false);
        this.balances.clear();
        HashMap<Currency, Integer> balances = accountState.getAllBalances();
        for (Currency curr : balances.keySet()) {
            setBalance(curr, balances.get(curr), false);
        }
        if (clearHistory) {
            this.history.clear();
        }
    }

    public void restoreAccountState(AccountState accountState) {
        restoreAccountState(accountState, true);
    }

    public boolean canUndo() {
        return !this.history.isEmpty();
    }

    public void undo() throws NoChangesToUndoException {
        if (!canUndo()) {
            throw new NoChangesToUndoException("Отсутствуют изменения по счёту.");
        }
        AccountState previousState = this.history.pop();
        restoreAccountState(previousState, false);
    }

    @Override
    public String toString() {
        return "Счёт. " +
                "Владелец='" + ownerName + '\'' +
                ", остатки=" + balances;
    }

    public static class AccountState {
        private final String ownerName;
        private final HashMap<Currency, Integer> balances;

        public AccountState(Account account) {
            this.ownerName = account.ownerName;
            this.balances = (HashMap<Currency, Integer>) account.balances.clone();
        }

        private String getOwnerName() {
            return this.ownerName;
        }

        private HashMap<Currency, Integer> getAllBalances() {
            return (HashMap<Currency, Integer>) this.balances.clone();
        }
    }
}
