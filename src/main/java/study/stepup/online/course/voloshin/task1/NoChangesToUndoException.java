package study.stepup.online.course.voloshin.task1;

public class NoChangesToUndoException extends Exception {

    public NoChangesToUndoException(String message) {
        super(message);
    }
}
