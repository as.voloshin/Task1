package study.stepup.online.course.voloshin.task1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;

public class Tests {

    @Test
    @DisplayName("Проверяем создание счета")
    public void checkAccountCreation() {
        Assertions.assertThrowsExactly(IllegalArgumentException.class, () -> {
            Account acc = new Account("");
        }, "Имя владельца счёта не может быть null или пустым.");
        Assertions.assertThrowsExactly(IllegalArgumentException.class, () -> {
            Account acc = new Account(null);
        }, "Имя владельца счёта не может быть null или пустым.");
        Assertions.assertAll(() -> {
            Account acc = new Account("Alexey");
        }, () -> {
            Account acc = new Account("Алексей");
        }, () -> {
            Account acc = new Account("null");
        }, () -> {
            Account acc = new Account("2+2");
        });
    }

    @Test
    @DisplayName("Проверяем изменение владельца счета")
    public void checkAccountOwnerChange() {
        Account acc = new Account("TestOwner");
        Assertions.assertThrowsExactly(IllegalArgumentException.class,
                () -> acc.setOwnerName(""),
                "Имя владельца счёта не может быть null или пустым.");
        Assertions.assertThrowsExactly(IllegalArgumentException.class,
                () -> acc.setOwnerName(null),
                "Имя владельца счёта не может быть null или пустым.");
        Assertions.assertAll(() -> acc.setOwnerName("Alexey"),
                () -> acc.setOwnerName("Алексей"),
                () -> acc.setOwnerName("null"),
                () -> acc.setOwnerName("2+2"));
    }

    @Test
    @DisplayName("Проверяем получение владельца счета")
    public void checkAccountOwnerGet() {
        Account acc1 = new Account("TestOwner");
        Account acc2 = new Account("Alexey");
        Account acc3 = new Account("Алексей");
        Assertions.assertEquals("TestOwner", acc1.getOwnerName());
        Assertions.assertEquals("Alexey", acc2.getOwnerName());
        Assertions.assertEquals("Алексей", acc3.getOwnerName());
        Assertions.assertNotEquals("Alexey", acc1.getOwnerName());
        Assertions.assertNotEquals("Алексей", acc2.getOwnerName());
        Assertions.assertNotEquals("TestOwner", acc3.getOwnerName());
        acc1.setOwnerName("Alexey");
        Assertions.assertEquals("Alexey", acc1.getOwnerName());
    }

    @Test
    @DisplayName("Проверяем изменение остатка")
    public void checkAccountBalanceChange() {
        Account acc = new Account("TestOwner");
        Assertions.assertThrowsExactly(IllegalArgumentException.class,
                () -> acc.setBalance(Currency.USD, -1),
                "Количество валюты не может быть отрицательным.");
        Assertions.assertThrowsExactly(IllegalArgumentException.class,
                () -> acc.setBalance(Currency.RUB, -9999),
                "Количество валюты не может быть отрицательным.");
        Assertions.assertAll(() -> acc.setBalance(Currency.USD, 0),
                () -> acc.setBalance(Currency.USD, 10),
                () -> acc.setBalance(Currency.USD, 500),
                () -> acc.setBalance(Currency.RUB, 0),
                () -> acc.setBalance(Currency.EUR, 0),
                () -> acc.setBalance(Currency.CNY, 0));
    }

    @Test
    @DisplayName("Проверяем получение остатка")
    public void checkAccountBalanceGet() {
        Account acc = new Account("TestOwner");
        Assertions.assertNull(acc.getBalance(Currency.RUB));
        Assertions.assertNull(acc.getBalance(Currency.USD));
        Assertions.assertNull(acc.getBalance(Currency.EUR));
        Assertions.assertEquals(0, acc.getAllBalances().size());
        acc.setBalance(Currency.RUB, 41);
        Assertions.assertEquals(41, acc.getBalance(Currency.RUB));
        Assertions.assertNull(acc.getBalance(Currency.USD));
        Assertions.assertNull(acc.getBalance(Currency.EUR));
        Assertions.assertEquals(1, acc.getAllBalances().size());
        acc.setBalance(Currency.EUR, 320);
        Assertions.assertEquals(41, acc.getBalance(Currency.RUB));
        Assertions.assertNull(acc.getBalance(Currency.USD));
        Assertions.assertEquals(320, acc.getBalance(Currency.EUR));
        Assertions.assertEquals(2, acc.getAllBalances().size());
        acc.setBalance(Currency.RUB, 42);
        Assertions.assertEquals(42, acc.getBalance(Currency.RUB));
        Assertions.assertNull(acc.getBalance(Currency.USD));
        Assertions.assertEquals(320, acc.getBalance(Currency.EUR));
        Assertions.assertEquals(2, acc.getAllBalances().size());
    }

    @Test
    @DisplayName("Проверяем инкапсуляцию остатков")
    public void checkBalancesEncapsulation() {
        Account acc = new Account("TestOwner");
        acc.setBalance(Currency.USD, 10);
        HashMap<Currency, Integer> balances = acc.getAllBalances();
        balances.put(Currency.USD, 1000000);
        Assertions.assertEquals(10, acc.getBalance(Currency.USD));
    }

    @Test
    @DisplayName("Проверяем метод undo")
    public void checkUndoMethod() {
        Account acc = new Account("TestOwner");
        Assertions.assertFalse(acc.canUndo());
        Assertions.assertThrowsExactly(NoChangesToUndoException.class, acc::undo, "Отсутствуют изменения по счёту.");
        acc.setBalance(Currency.RUB, 100);
        acc.setOwnerName("Иванов");
        acc.setBalance(Currency.RUB, 300);

        Assertions.assertTrue(acc.canUndo());
        Assertions.assertAll(acc::undo);
        Assertions.assertEquals("Иванов", acc.getOwnerName());
        Assertions.assertEquals(100, acc.getBalance(Currency.RUB));

        Assertions.assertTrue(acc.canUndo());
        Assertions.assertAll(acc::undo);
        Assertions.assertEquals("TestOwner", acc.getOwnerName());
        Assertions.assertEquals(100, acc.getBalance(Currency.RUB));

        Assertions.assertTrue(acc.canUndo());
        Assertions.assertAll(acc::undo);
        Assertions.assertEquals("TestOwner", acc.getOwnerName());
        Assertions.assertNull(acc.getBalance(Currency.RUB));

        Assertions.assertFalse(acc.canUndo());
    }

    @Test
    @DisplayName("Проверяем сохранение состояния счёта")
    public void checkSaveAccountStateMethod() {
        Account acc = new Account("TestOwner");
        acc.setBalance(Currency.RUB, 100);
        Assertions.assertAll(() -> {
            Account.AccountState state1 = acc.saveAccountState();
        });
        acc.setBalance(Currency.USD, 10);
        acc.setOwnerName("Виктор");
        Assertions.assertAll(() -> {
            Account.AccountState state2 = acc.saveAccountState();
        });
    }

    @Test
    @DisplayName("Проверяем иммутабельность состояния счёта")
    public void checkAccountStateIsImmutable() {
        for (Field field : Account.AccountState.class.getDeclaredFields()) {
            Assertions.assertTrue(Modifier.isFinal(field.getModifiers()));
            Assertions.assertTrue(Modifier.isPrivate(field.getModifiers()));
        }
    }

    @Test
    @DisplayName("Проверяем восстановление состояния счёта")
    public void checkRestoreAccountStateMethod() {
        Account acc = new Account("TestOwner");
        acc.setBalance(Currency.RUB, 100);
        Account.AccountState state1 = acc.saveAccountState();
        acc.setOwnerName("Иванов");
        Account.AccountState state2 = acc.saveAccountState();
        acc.setBalance(Currency.RUB, 300);

        Assertions.assertAll(() -> acc.restoreAccountState(state1));
        Assertions.assertEquals("TestOwner", acc.getOwnerName());
        Assertions.assertEquals(100, acc.getBalance(Currency.RUB));

        Assertions.assertAll(() -> acc.restoreAccountState(state2));
        Assertions.assertEquals("Иванов", acc.getOwnerName());
        Assertions.assertEquals(100, acc.getBalance(Currency.RUB));
    }

}
